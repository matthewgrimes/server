apiVersion: v1
kind: Namespace
metadata:
  name: wordpress
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress
  namespace: wordpress
  labels:
    app: wordpress
spec:
  replicas: 1
  selector:
    matchLabels:
      pod-label: wordpress-server-pod
  template:
    metadata:
      labels:
        pod-label: wordpress-server-pod
    spec:
      containers:
        - name: wordpress
          image: wordpress:latest
          volumeMounts:
          - name: pv-wordpress-data
            mountPath: /var/www/html/wp-content
          env:
          - name: WORDPRESS_DB_NAME
            value: wordpress
          - name: WORDPRESS_DB_HOST
            value: wordpress-db
          envFrom:
          - secretRef:
              name: wordpress-db-secret
      volumes:
      - name: pv-wordpress-data
        persistentVolumeClaim:
          claimName: pvc-wordpress-data
---
apiVersion: v1
kind: Service
metadata:
  name: wordpress-server
  namespace: wordpress
  labels:
    app: wordpress
spec:
  selector:
    pod-label: wordpress-server-pod
  ports:
  - protocol: TCP
    port: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: wordpress
  namespace: wordpress
  annotations:
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
    - hosts:
      - mtgrimes.com
      secretName: wordpress-tls
  ingressClassName: nginx
  rules:
  - host: mtgrimes.com
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: wordpress-server
            port:
              number: 80
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-wordpress-data
  namespace: wordpress
spec:
  storageClassName: longhorn
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress-db
  namespace: wordpress
  labels:
    app: wordpress
spec: 
  replicas: 1
  selector:
    matchLabels:
      pod-label: wordpress-db-pod
  template:
    metadata:
      labels:
        pod-label: wordpress-db-pod
    spec:
      containers:
      - name: mysql
        image: mysql:8-oracle
        env:
        - name: MYSQL_DATABASE
          value: wordpress
        envFrom:
        - secretRef:
            name: wordpress-db-secret
        volumeMounts:
        - name: db-storage
          mountPath: /var/lib/mysql
          subPath: mysql-data
      volumes:
      - name: db-storage
        persistentVolumeClaim:
          claimName: wordpress-mysql-claim
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: wordpress-mysql-claim
  namespace: wordpress
  labels:
    app: wordpress
spec:
  storageClassName: longhorn
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
---
apiVersion: v1
kind: Service
metadata:
  name: wordpress-db
  namespace: wordpress
  labels:
    app: wordpress
spec:
  selector:
    pod-label: wordpress-db-pod
  ports:
  - protocol: TCP
    port: 3306
